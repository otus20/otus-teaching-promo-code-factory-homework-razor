﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
   public class CustomersController : Controller
   {
      private readonly Api.CustomersController _customersController;
      private readonly Api.PreferencesController _preferencesController;

      public CustomersController(Api.CustomersController customersController,
         Api.PreferencesController preferencesController)
      {
         _customersController = customersController;
         _preferencesController = preferencesController;
      }

      public async Task<IActionResult> Index()
      {
         var result = await _customersController.GetCustomersAsync();
         var customers = ((OkObjectResult)result.Result).Value;

         return View("Index", customers);
      }
      
      [HttpPost]
      public async Task<IActionResult> Create(CreateOrEditCustomerRequest customer)
      {
         if (ModelState.IsValid)
         {
            await _customersController.CreateCustomerAsync(customer);
         }

         return Redirect(nameof(Index));
      }

      public async Task<IActionResult> Edit(Guid id)
      {
         var customerResult = await _customersController.GetCustomerAsync(id);
         var customer = (CustomerResponse)((OkObjectResult)customerResult.Result).Value;

         var allPreferencesResult = await _preferencesController.GetPreferencesAsync();
         var allPreferences = (List<PreferenceResponse>)((OkObjectResult)allPreferencesResult.Result).Value;

         customer.AllPreferences = new MultiSelectList(allPreferences, "Id", "Name");
         customer.SelectedPreferences = customer.Preferences.Select(x => x.Id).ToList();

         return View(customer);
      }

      [HttpPost]
      public async Task<IActionResult> Edit(Guid id, CustomerResponse customer)
      {
         if (ModelState.IsValid)
         {
            var editRequest = new CreateOrEditCustomerRequest
            {
               FirstName = customer.FirstName,
               LastName = customer.LastName,
               Email = customer.Email,
               PreferenceIds = customer.SelectedPreferences
            };

            await _customersController.EditCustomersAsync(id, editRequest);
            return RedirectToAction(nameof(Index));
         }

         return await Edit(id);
      }
      
      public async Task<IActionResult> Delete(Guid id)
      {
         var result = await _customersController.GetCustomerAsync(id);
         var customer = ((OkObjectResult)result.Result).Value;

         return View(customer);
      }

      [HttpPost, ActionName("Delete")]
      public async Task<IActionResult> DeleteCustomer(Guid id)
      {
         await _customersController.DeleteCustomerAsync(id);
         return RedirectToAction(nameof(Index));
      }
   }
}