﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
   public class PreferencesController : Controller
   {
      private readonly Api.PreferencesController _preferencesController;

      public PreferencesController(Api.PreferencesController preferencesController)
      {
         _preferencesController = preferencesController;
      }

      public async Task<IActionResult> Index()
      {
         var result = await _preferencesController.GetPreferencesAsync();
         var preferences = ((OkObjectResult)result.Result).Value;

         return View("Index", preferences);
      }
   }
}