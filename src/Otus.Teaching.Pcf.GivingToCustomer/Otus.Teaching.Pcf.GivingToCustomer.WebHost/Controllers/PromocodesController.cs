﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
   public class PromocodesController : Controller
   {
      private readonly Api.PromocodesController _promocodesController;

      public PromocodesController(Api.PromocodesController promocodesController)
      {
         _promocodesController = promocodesController;
      }

      public async Task<IActionResult> Index()
      {
         var result = await _promocodesController.GetPromocodesAsync();
         var promocodes = ((OkObjectResult)result.Result).Value;

         return View("Index", promocodes);
      }
   }
}