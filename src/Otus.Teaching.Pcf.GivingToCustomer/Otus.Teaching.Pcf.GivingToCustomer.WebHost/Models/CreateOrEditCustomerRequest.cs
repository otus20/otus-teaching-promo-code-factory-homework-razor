﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    /// <example>
    /// {
    ///    "firstName": "Иван",
    ///    "lastName": "Васильев",
    ///     "email": "ivan_vasiliev@somemail.ru",
    ///        "preferenceIds": [
    ///            "c4bda62e-fc74-4256-a956-4760b3858cbd"
    ///        ]
    /// }
    /// </example>>
    public class CreateOrEditCustomerRequest
    {
        [Required]
        [MaxLength(100, ErrorMessage = "Max 100")]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(100, ErrorMessage = "Max 100")]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }
    }
}