﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class CustomerMapper
    {
       public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences,
          Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            if (preferences != null)
            {
               var newPreferences = preferences.Select(x => new CustomerPreference()
               {
                  CustomerId = customer.Id,
                  Preference = x,
                  PreferenceId = x.Id
               }).ToList();

               if (customer.Preferences != null)
               {
                  customer.Preferences = customer.Preferences
                     .Where(x => newPreferences.Any(p => p.PreferenceId == x.PreferenceId))
                     .ToList();

                  newPreferences = newPreferences
                     .Where(x => customer.Preferences.All(p => p.PreferenceId != x.PreferenceId))
                     .ToList();

                  newPreferences.AddRange(customer.Preferences);
               }

               customer.Preferences = newPreferences;
            }

            return customer;
        }
    }
}
