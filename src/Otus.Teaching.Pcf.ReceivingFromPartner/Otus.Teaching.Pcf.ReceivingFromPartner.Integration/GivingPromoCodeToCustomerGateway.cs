﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
   public class GivingPromoCodeToCustomerGateway : IGivingPromoCodeToCustomerGateway
   {
      private readonly IOptions<RabbitMqConfiguration> _rabbitMqConfiguration;
      private IConnection _connection;

      public GivingPromoCodeToCustomerGateway(IOptions<RabbitMqConfiguration> rabbitMqOptions)
      {
         _rabbitMqConfiguration = rabbitMqOptions;
         CreateConnection();
      }

      public Task GivePromoCodeToCustomer(PromoCode promoCode)
      {
         if (ConnectionExists())
         {
            var dto = new GivePromoCodeToCustomerDto()
            {
               PartnerId = promoCode.Partner.Id,
               BeginDate = promoCode.BeginDate.ToShortDateString(),
               EndDate = promoCode.EndDate.ToShortDateString(),
               PreferenceId = promoCode.PreferenceId,
               PromoCode = promoCode.Code,
               ServiceInfo = promoCode.ServiceInfo,
               PartnerManagerId = promoCode.PartnerManagerId
            };

            using var channel = _connection.CreateModel();

            channel.QueueDeclarePassive(_rabbitMqConfiguration.Value.PromocodesQueue);

            var json = JsonConvert.SerializeObject(dto);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(
               exchange: string.Empty,
               routingKey: _rabbitMqConfiguration.Value.PromocodesQueue,
               basicProperties: null,
               body: body);
         }

         return Task.CompletedTask;
      }

      private void CreateConnection()
      {
         try
         {
            var factory = new ConnectionFactory
            {
               HostName = _rabbitMqConfiguration.Value.Hostname,
               UserName = _rabbitMqConfiguration.Value.UserName,
               Password = _rabbitMqConfiguration.Value.Password
            };

            _connection = factory.CreateConnection();
         }
         catch (Exception ex)
         {
            // ignored
         }
      }

      private bool ConnectionExists()
      {
         if (_connection != null)
            return true;

         CreateConnection();

         return _connection != null;
      }
   }
}