﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Exceptions;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
   public class AppliedPromoCodeManager : IAppliedPromoCodeManager
   {
      private readonly IRepository<Employee> _employeeRepository;

      public AppliedPromoCodeManager(IRepository<Employee> employeeRepository)
      {
         _employeeRepository = employeeRepository;
      }

      public async Task UpdateManagerPromoCodes(Guid managerId)
      {
         var employee = await _employeeRepository.GetByIdAsync(managerId);

         if (employee == null)
            throw new EmployeeNotFoundException();

         employee.AppliedPromocodesCount++;

         await _employeeRepository.UpdateAsync(employee);
      }
   }
}